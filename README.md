# Proj0-Hello
-------------

Simple program that prints "Hello world"
Author: Dominic Vicharelli
Contact: dominicv@uoregon.edu

## Instructions:
---------------

- Clone this repo with the following command:
``` git clone https://dominicv26@bitbucket.org/dominicv26/proj0-hello.git```

- Type the command: ```make run``` to execute.

